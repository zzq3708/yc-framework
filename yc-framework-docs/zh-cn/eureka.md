# Eureka
只需引入如下依赖即可:
```
<dependency>
    <groupId>com.yc.framework</groupId>
    <artifactId>yc-common-eureka</artifactId>
</dependency>
```

# YC-Framework如何使用Eureka
[轻松搭建SpringCloud Eureka架构体系](https://youcongtech.com/2022/04/15/%E8%BD%BB%E6%9D%BE%E6%90%AD%E5%BB%BASpringCloud-Eureka%E6%9E%B6%E6%9E%84%E4%BD%93%E7%B3%BB/)